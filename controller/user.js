const UserModel = require("../model/user.js")

class User {

  static getUser(req, res, next) {
    try {
      const _user = UserModel.getAllUser()
      res.status(200).json(_user)
    } catch (error) {
      next(error)
    }
  }

  static getOneUser(req, res, next) {
    try {
      const { id } = req.params
      const user = UserModel.getOneUser(id)
      res.status(200).json(user)
    } catch (error) {
      next(error)
    }
  }

  static login(req, res, next) {
    try {
      const { password, name } = req.body
      let idx = user.findIndex(x => x.name === name);
      console.log(idx, "<<< INDEX USER")
      console.log(password, name, "<< DATA USER")
      // console.log(user[idx].password ==)
      const checkPassword = password == user[idx].password
      if (checkPassword == false) {
        throw {
          status: 401,
          message: "anda tidak memiliki akses"
        }
      } else {
        const _name = user[idx].name
        res.status(200).json({
          name: _name
        })
      }
    } catch (error) {
      console.log(error)
      next(error)
    }
  }
}


module.exports = User