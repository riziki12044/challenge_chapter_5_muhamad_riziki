class Home {
  // View
  static sendView(req, res, next) {
    const payload = {
      name: "ikhy",
      address: "Ambon",
      friends: ["Nanhy", "Vidia", "Filani"]
    }
    res.render("index", payload)
  }

  static chap4(req, res, next) {
    console.log("=== chapter_4 ====")
    res.render("chap4")
  }

  static loginPage(req, res, next) {
    res.render("login")
  }

  static registerFormView(req, res, next) {
    res.render("register")
  }
  // Models
  static getHome(req, res, next) {
    console.log("=== Controller ===")
    try {
      res.send("GET HOME")
      // throw "CUSTOM ERROR"

    } catch (error) {
      console.log(error, "<<<< Eror")
      next(error)
    }
  }

  static editHome(req, res, next) {
    try {
      res.send("Edit HOME")

    } catch (error) {
      next(error)
    }

  }

  static deleteHome(req, res, next) {
    try {
      res.send("DELETE HOME")
    } catch (error) {
      next(error)
    }

  }

  static postHome(req, res, next) {
    try {
      res.send("POST HOME")
    } catch (error) {
      next(error)
    }
  }

}

module.exports = Home