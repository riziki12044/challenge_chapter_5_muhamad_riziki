const user = require("./user.json")
class UserModel {

  static getAllUser() {
    const _user = user

    _user.forEach(el => {
      delete el.password
    })

    return _user

  }

  static getOneUser(id) {
    const _user = user
    const idx = _user.findIndex(x => x.id == id)
    const finalData = _user[idx]

    delete finalData.password
    return finalData

  }
}

module.exports = UserModel