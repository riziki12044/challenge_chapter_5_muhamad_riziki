const route = require("express").Router()
const { Home, User } = require("../controller")
const Middleware = require("../middleware")


// Home Route
route.get("/", Home.sendView)
route.get("/register", Home.registerFormView)
route.get("/chap4", Middleware.checkLocalStorage, Home.chap4)
route.get("/login", Home.loginPage)
route.post("/login", User.login)
route.get('/home', Middleware.putMiddleware, Home.getHome)
route.post('/', Home.postHome)
route.put('/', Home.editHome)
route.delete('/', Home.deleteHome)


// pengguna Route
route.get("/user", User.getUser)
route.get("/user/:id", User.getOneUser)



module.exports = route