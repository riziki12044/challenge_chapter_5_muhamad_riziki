
class Middleware {
  static putMiddleware(req, res, next) {
    console.log("=== Funsi Middleware  ===")
    console.log(req.body)
    const name = req.body.name
    next()
    
  }

  static errorHandler(err, req, res, next) {
    console.log("=== error handler ===")
    console.log(err)
    if (err.status) {
      const _status = err.status
      res.status(_status).json(err)
    }
    else {
      res.status(400).json({
        messages: "Ooppss Halaman tujuan tidak di temukan"
      })
    }
  }

  static checkLocalStorage(req, res, next) {
    console.log("=== check ===")
    const _local = localStorage.getItem("is Login")

    console.log(_local, "<<<")
    next()
  }
}

module.exports = Middleware