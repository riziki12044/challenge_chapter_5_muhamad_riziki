const express = require('express')
const app = express()
const port = 3000
const routes = require("./routes/index")
const Middleware = require("./middleware/index")

app.use(express.static('views/assets'))
app.set("view engine", "ejs")

app.use(express.json())
app.use(express.urlencoded({ extended: true }))

app.use(routes)
app.use(Middleware.errorHandler)

app.all("*", (req, res) => {
  res.send("404 Page Not Found")
})

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})

